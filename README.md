# GameOfDronesFront

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

#FRONT-END instructions
Front-end was developed with Angular 7
In order to make this application work , you should follow the next steps:
1.install node.js
2.install angular CLI
3.execute command npm install --save-dev to install dependencies
3 execute command ng serve --open 


this project was developed with the folowing versions angular :7.2,HTML 5, python 3 ,django 2.1.5
on a macbook with high sierre IOS
