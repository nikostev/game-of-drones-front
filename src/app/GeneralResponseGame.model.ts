import { Game } from './Game.model';
export interface GeneralResponseGame {
	status: number,
	data: Game[],
}