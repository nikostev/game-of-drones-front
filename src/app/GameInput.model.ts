export interface GameInput {
	playerOne: string,
	playerTwo: string,
}