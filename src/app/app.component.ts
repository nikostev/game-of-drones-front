import { Component, OnInit } from '@angular/core';
import { GameInput } from './GameInput.model';
import { GeneralResponseGame } from './GeneralResponseGame.model';
import { RestService } from './rest.service';
import { MoveInput } from './MoveInput.model';
import { GeneralResponseMove } from './GeneralResponseMove.model';
import { Move } from './Move.model';
import { GeneralResponseStatistics } from './GeneralResponseStatistics.model';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent implements OnInit  {
  title = 'game-of-drones-front';
  inputGame: GameInput;
  mError : string;
  inGame: boolean;
  game:  GeneralResponseGame;
  turnPlayerName: string;
  turnPlayer: number;
  winnerP1: number;
  winnerP2: number;
  moveinput: MoveInput;
  lastMove: GeneralResponseMove;
  mOk:  string;
  round: number;
  isFinish: boolean;
  mWinner:  string;
  statistics: GeneralResponseStatistics;

	constructor(private service: RestService) { 
	     
	}

	ngOnInit() {
		this.inputGame = {
			playerOne: '',
		  playerTwo: '',
		}

		this.game = {
			status: 0,
			data: []
		}

    this.lastMove = {
      status: 0,
      data: []
    }

    this.moveinput = {
      playerId: 0,
      gameId: 0,
      move: "ND",
    }

    this.statistics = {
      status: 0,
      data: []
    }

		this.mError = "";
		this.turnPlayerName = "";
		this.inGame = false;
		this.turnPlayer =  1;
		this.winnerP1 = 0;
		this.winnerP2 = 0;
    this.mOk = "";
    this.round  = 1;
    this.isFinish = false;
    this.mWinner = "";

    this.gamesPerPlayer();
	}


InitPlayValidate(): boolean {
	if (this.inputGame.playerOne!=="" && this.inputGame.playerTwo!=="") {
		return true;
	} else {
		return false;
	}
}

reset(): void{
    this.inputGame = {
      playerOne: '',
      playerTwo: '',
    }

    this.game = {
      status: 0,
      data: []
    }

    this.statistics = {
      status: 0,
      data: []
    }
    
    this.lastMove = {
      status: 0,
      data: []
    }

    this.moveinput = {
      playerId: 0,
      gameId: 0,
      move: "ND",
    }

    this.mError = "";
    this.turnPlayerName = "";
    this.inGame = false;
    this.turnPlayer =  1;
    this.winnerP1 = 0;
    this.winnerP2 = 0;
    this.mOk = "";
    this.round  = 1;
    this.isFinish = false;
    this.mWinner = "";
    this.gamesPerPlayer();
}


move(move: string ): void {
    let idPlayer : number;

    if (this.turnPlayer == 1){
      idPlayer = this.game.data[0].playerOneId
    }else{
      idPlayer = this.game.data[0].playerTwoId
    }
    

     this.moveinput = {
        playerId: idPlayer,
        gameId: this.game.data[0].gameId,
        move: move,
     }

    this.service.move(this.moveinput).subscribe(
      (val) => {
      	this.lastMove = val;
      	this.mError ="";
      	if (this.lastMove.status===200){
      
           
            if (this.turnPlayer === 1){
               this.turnPlayerName =this.inputGame.playerTwo;
               this.turnPlayer = 2
            }else{
               this.turnPlayerName =this.inputGame.playerOne;
               this.turnPlayer = 1
            }

            if (this.lastMove.data[0].roundWinner === "P1" ){
                this.winnerP1 ++;
                this.mOk  = this.inputGame.playerOne+" won the round";
                this.round ++;
            }else if (this.lastMove.data[0].roundWinner === "P2" ){
                this.winnerP2 ++;
                this.mOk  =this.inputGame.playerTwo+" won the round";
                this.round ++;
            }else if(this.lastMove.data[0].roundWinner === "DW") {
              this.mOk  ="Draw";
              this.round ++;
            }else{
              this.mOk  ="";
            }


            if (this.lastMove.data[0].winner === "P1" ){
              this.mWinner  = this.inputGame.playerOne+" is the emperor";
              this.isFinish  = true;
            }

            if (this.lastMove.data[0].winner === "P2" ){
              this.mWinner  =this.inputGame.playerTwo+" is the emperor";
              this.isFinish  = true;
            }

      	}else{
      		this.mError ="An error has occurred";
      	}

      	console.table(this.game);
      },
      response => {
       // this.showResponse(response);
      });

  
  }


  newGame(): void {
    if (this.InitPlayValidate()) {
      this.service.createGame(this.inputGame).subscribe(
        (val) => {
        	this.game = val;
        	this.mError ="";
        	if (this.game.status===200){
        		this.inGame = true;
        		this.turnPlayer = 1;
        		this.turnPlayerName  = this.inputGame.playerOne;
        	}else{
        		this.mError ="An error has occurred";
        	}

        	console.table(this.game);
        },
        response => {
         // this.showResponse(response);
        });
    }else{
      this.mError = "Type players names";
    }
  
  }


  gamesPerPlayer(): void {

      this.service.gamesPerPlayer().subscribe(
        (val) => {
          this.statistics = val;
          this.mError ="";
          if (this.statistics.status!==200){
            this.mError ="An error has occurred";
          }

          console.table(this.game);
        },
        response => {
         // this.showResponse(response);
        });

  
  }
}

