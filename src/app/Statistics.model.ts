export interface Statistics {
	player: string,
	total: number,
}