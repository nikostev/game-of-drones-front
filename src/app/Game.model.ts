export interface Game {
	playerTwoId: number,
	playerOneId: number,
	gameId: number,
}