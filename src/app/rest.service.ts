import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { GameInput } from './GameInput.model';
import { GeneralResponseGame } from './GeneralResponseGame.model';
import { MoveInput } from './MoveInput.model';
import { GeneralResponseMove } from './GeneralResponseMove.model';
import { GeneralResponseStatistics } from './GeneralResponseStatistics.model';

@Injectable({
  providedIn: 'root'
})
export class RestService {

constructor(private http: HttpClient) { }


 endpoint = 'http://localhost:8000/';
 httpOptions = {
 headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

createGame(gameInput: GameInput): Observable<GeneralResponseGame> {
	console.table(gameInput);
	return this.http.post<GeneralResponseGame>(this.endpoint + 'game/', gameInput, this.httpOptions );
}

move(moveInput: MoveInput): Observable<GeneralResponseMove> {
	console.table(moveInput);
	return this.http.post<GeneralResponseMove>(this.endpoint + 'play/', moveInput, this.httpOptions );
}

gamesPerPlayer(): Observable<GeneralResponseStatistics> {
	return this.http.get<GeneralResponseStatistics>(this.endpoint + 'gamesPerPlayer/', this.httpOptions );
}

}
