import { Move } from './Move.model';
export interface GeneralResponseMove {
	status: number,
	data: Move[],
}