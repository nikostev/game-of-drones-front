export interface Move {
	winner: string,
	roundGameId: number,
	gameId: number,
	roundWinner: string,
}