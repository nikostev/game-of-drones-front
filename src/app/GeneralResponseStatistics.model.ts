
import { Statistics } from './Statistics.model';
export interface GeneralResponseStatistics {
	status: number,
	data: Statistics[],
}