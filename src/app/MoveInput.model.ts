export interface MoveInput {
	playerId: number,
	gameId: number,
	move: string,
}